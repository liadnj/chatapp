import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AngularFireModule } from 'angularfire2';
import {AF} from "./providers/af";
import { LoginPageComponent } from './login-page/login-page.component';
import { HomePageComponent } from './home-page/home-page.component';
import {RouterModule, Routes} from "@angular/router";
import {FormsModule} from "@angular/forms";
import { RegistrationPageComponent } from './registration-page/registration-page.component';

// Must export the config
export const firebaseConfig = {
   apiKey: "AIzaSyBZN3CRkFB7ZqHIW-u5BBemlfmQVi7q3LI",
    authDomain: "angularchat-6ea82.firebaseapp.com",
    databaseURL: "https://angularchat-6ea82.firebaseio.com",
    projectId: "angularchat-6ea82",
    storageBucket: "angularchat-6ea82.appspot.com",
    messagingSenderId: "304640458388"
};

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'login', component: LoginPageComponent },
   { path: 'register', component: RegistrationPageComponent}
];
@NgModule({
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig),
    RouterModule.forRoot(routes),
    FormsModule
  ],
  declarations: [ AppComponent, LoginPageComponent, HomePageComponent, RegistrationPageComponent ],
  bootstrap: [ AppComponent ],
  providers: [AF]
})
export class AppModule {}